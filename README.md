React native uygulamasını çalıştırmak için ihtiyacınız olan tek şey expo-cli olacak.

npm install --global expo-cli

ile expo-cli kurulumunu yapabilirsiniz, ardından terminalden dosya içine girip "expo start" komutunu çalıştırdığınızda, karşınıza çıkan expo panelinden uygulamayı kendi telefonunuzda çalıştırabilirsiniz.

Bunun için telefonunuza "expo" uygulamasını yüklemeniz, ekranda çıkan qr kodu okutmanız ve bilgisayarınız ile aynı ağa bağlı olmanız gerekmektedir.
