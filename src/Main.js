import React, { Component } from "react";
import {
  Alert,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from "react-native";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";
import { icons } from "./theme/icons";
import _ from "lodash";

import { LineChart } from "react-native-chart-kit";

const { width, height } = Dimensions.get("window");

const CountryData = ["Belarus", "Brazil", "Canada", "Germany", "Turkey"];

const MonthNames = [
  "Ocak",
  "Şubat",
  "Mart",
  "Nisan",
  "Mayıs",
  "Haziran",
  "Temmuz",
  "Ağustos",
  "Eylül",
  "Ekim",
  "Kasım",
  "Aralık",
];

const firstData = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
    },
  ],
};

export default class Main extends Component {
  state = { data: [], selectedCountry: "", chartData: [] };

  componentDidMount() {}

  selectCountry = (country) => {
    this.setState({ selectedCountry: country }, () => this.getData(country));
  };

  getData = async (country) => {
    const response = await fetch(
      `https://www.statbureau.org/get-data-json/?country=${country}`,
      {
        method: "GET",
      }
    );
    var resData = await response.json();
    console.log(resData);
    var filteredData = resData.filter((item) => {
      return item.MonthFormatted.includes("2018");
    });

    var dataSetValues = [];

    for (var i = 0; i < filteredData.length; i++) {
      dataSetValues.push(filteredData[i].InflationRateRounded);
    }

    console.log(dataSetValues);
    var chartData = {
      labels: MonthNames,
      datasets: [
        { data: dataSetValues },
        {
          data: [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
        },
      ],
    };
    console.log(chartData);
    this.setState({ chartData: chartData }, console.log(this.state.chartData));
  };

  renderCountries = (item, index) => {
    const { selectedCountry } = this.state;
    return (
      <TouchableOpacity
        onPress={() => this.selectCountry(item)}
        key={index}
        style={{
          padding: 10,
          backgroundColor: selectedCountry === item ? "red" : "#3d3d3d",
          margin: 2,
          borderRadius: 5,
        }}
      >
        <Text style={styles.text}>{item}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaProvider style={styles.screen}>
        <SafeAreaView style={styles.container}>
          <View style={styles.countries}>
            {CountryData.map((item, index) => {
              return this.renderCountries(item, index);
            })}
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: "green",
              width: "100%",
              borderWidth: StyleSheet.hairlineWidth,
              padding: 10,
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              borderTopWidth: 0,
            }}
          >
            <Text style={{ fontSize: 18, fontWeight: "bold", color: "white" }}>
              Getir
            </Text>
            <Image source={icons.filter} style={{ tintColor: "white" }} />
          </TouchableOpacity>
          {!_.isEmpty(this.state.chartData) && (
            <LineChart
              data={this.state.chartData}
              width={width}
              height={200}
              chartConfig={{
                backgroundColor: "#e26a00",
                backgroundGradientFrom: "#fb8c00",
                backgroundGradientTo: "#ffa726",
                decimalPlaces: 2, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 16,
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: "#ffa726",
                },
              }}
            />
          )}
        </SafeAreaView>
      </SafeAreaProvider>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    width: "100%",
  },
  container: {
    flex: 1,
    backgroundColor: "gray",

    alignItems: "center",
  },
  countries: {
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    borderWidth: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomWidth: 0,
    backgroundColor: "lightgray",
  },
  text: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
  },
});
